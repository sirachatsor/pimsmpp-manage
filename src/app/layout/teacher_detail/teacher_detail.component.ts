import { Component, OnInit} from "@angular/core";
import { CallAPIService } from "../../call-api.service";


@Component({
    selector: 'app-teacher_detail',
    templateUrl: './teacher_detail.component.html',
    styleUrls: ['./teacher_detail.component.scss']
})
export class TeacherDetailComponent implements OnInit {
    constructor(private callApi : CallAPIService) {}
    model:any;
    id:any
    ngOnInit() {
        this.id = localStorage.getItem('idLogin')
        this.callApi.call("post","teacher/detail",{teach_code:this.id}).then(res =>{
            
            this.model = res["data"][0];
            console.log(this.model);
          });
    }
}
