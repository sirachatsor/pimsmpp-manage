import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeacherDetailRoutingModule } from './teacher_detail-routing.module';
import { TeacherDetailComponent } from './teacher_detail.component';
import { PageHeaderModule } from '../../shared';
import { DataTablesModule } from 'angular-datatables';
import { CallAPIService } from '../../call-api.service';


@NgModule({
    imports: [CommonModule, TeacherDetailRoutingModule ,PageHeaderModule,DataTablesModule],
    providers: [CallAPIService],
    declarations: [TeacherDetailComponent]
})
export class TeacherDetailModule {}
