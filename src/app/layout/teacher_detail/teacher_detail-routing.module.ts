import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TeacherDetailComponent } from './teacher_detail.component';

const routes: Routes = [
    {
        path: '',
        component: TeacherDetailComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TeacherDetailRoutingModule {}
