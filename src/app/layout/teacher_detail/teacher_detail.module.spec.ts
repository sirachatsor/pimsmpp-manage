import { TeacherDetailModule } from './teacher_detail.module';

describe('TeacherDetailModule', () => {
    let teacherModule: TeacherDetailModule;

    beforeEach(() => {
        teacherModule = new TeacherDetailModule();
    });

    it('should create an instance', () => {
        expect(teacherModule).toBeTruthy();
    });
});
