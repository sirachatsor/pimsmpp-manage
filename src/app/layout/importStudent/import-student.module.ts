import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImportStudentRoutingModule } from './import-student-routing.module';
import { ImportStudentComponent } from './import-student.component';
import { PageHeaderModule } from '../../shared';
import { DataTablesModule } from 'angular-datatables';
import { CallAPIService } from '../../call-api.service';
import { FormsModule }   from '@angular/forms';

@NgModule({
    imports: [CommonModule, ImportStudentRoutingModule,PageHeaderModule,DataTablesModule,FormsModule],
    declarations: [ImportStudentComponent],
    providers: [CallAPIService]
})
export class ImportStudentModule {}
