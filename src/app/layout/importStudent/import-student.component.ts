import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { DataTableDirective } from "angular-datatables";
import { ActivatedRoute } from "@angular/router";
import { CallAPIService } from "../../call-api.service";
import { ThrowStmt } from "@angular/compiler";
import {Location} from '@angular/common';
@Component({
  selector: "app-import-student",
  templateUrl: "./import-student.component.html",
  styleUrls: ["./import-student.component.scss"]
})
export class ImportStudentComponent implements OnInit {
  @ViewChild("inputFile") myInputVariable: ElementRef;
  params: any;

  datatableElement: DataTableDirective;
  data: any = { theader: [], tbody: [] };
  teacher: any;
  subject: any;
  jsonData: any;
  model: any = {};
  show_tbl = false;
  show_msg = false;
  msg: string = "";
  constructor(
    private _Activatedroute: ActivatedRoute,
    private callApi: CallAPIService,
    private _location: Location
  ) {}
  ngOnInit() {
    this._Activatedroute.queryParams.subscribe(queryParam => {
      this.params = queryParam["id"];
    });
    this.callApi
      .call("post", "getaddscorestudent", { teach_code: this.params })
      .then(res => {
        console.log("res", res);
        this.jsonData = res["data"];
        this.teacher = this.jsonData["teacher"][0];
        this.subject = this.jsonData["subject"];
        console.log(this.teacher);
      });
  }

  changeListener(files: FileList) {
    console.log(files);
    if (files && files.length > 0) {
      let file: File = files.item(0);
      console.log(file.name);
      console.log(file.size);
      console.log(file.type);
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = e => {
        this.msg = "";
        this.show_msg = false;
        let csv: any = reader.result;
        console.log(csv);
        let allTextLines = csv.split(/\r|\n|\r/);
        let headers = allTextLines[0].split(",");
        let bodies = [];
        let status = [];
        for (let i = 1; i < allTextLines.length; i++) {
          // split content based on comma
          let data = allTextLines[i].split(",");

          var validate_score = "fa fa-check co-green";
          if (data.length === headers.length) {
            let tarr = [];
            for (let j = 0; j < headers.length; j++) {
              // console.log("key is  : " + j + " value is : " + data[j]);

              if (
                (j >= 3 && j < headers.length - 1 && data[j] > 100) ||
                data[j] < 0
              ) {
                this.msg =
                  "*** คะแนนนักศึกษารหัสที่ " +
                  data[0] +
                  " ไม่ถูกต้อง รบกวนตรวจสอบข้อมูลและอัพโหลดไฟล์อีกครั้ง ***";
                this.show_msg = true;
                console.log(
                  "คะแนนผิดพลาด รบกวนตรวจสอบข้อมูลและ อัพโหลดไฟล์อีกครั้ง"
                );
                validate_score = "fa fa-times co-red";
              } else if (j == 0) {
                // console.log(bodies.length);
                // for (let a = 0; a < bodies.length; a++) {
                //   if(bodies[a][0] ==)
                //   console.log()
                //   console.log(bodies[a].includes(data[0]));
                // }
              }
              // console.log(bodies.includes(data[0]));
              tarr.push(data[j]);
              // console.log(data[0]);
              // var id =  data[0].toString();
            }

            status.push(validate_score);
            // log each row to see output
            console.log(tarr[0]);
            console.log(bodies.some(x => x[0] === tarr[0]));
            if (bodies.some(x => x[0] === tarr[0])) {
              this.msg =
                "*** พบรหัสนักศึกษา " +
                data[0] +
                " ซ้ำ รบกวนตรวจสอบข้อมูลและอัพโหลดไฟล์อีกครั้ง ***";
              this.show_msg = true;
            } else if (bodies.some(x => x[1] === tarr[1])) {
              this.msg =
                "*** พบชื่อนักศึกษา " +
                data[1] +
                " ซ้ำ รบกวนตรวจสอบข้อมูลและอัพโหลดไฟล์อีกครั้ง ***";
              this.show_msg = true;
            }
            console.log("Check duplicate");

            console.log("End check dupplicate");
            bodies.push(tarr);
          }
        }
        // all rows in the csv file
        this.show_tbl = true;
        this.data = { theader: headers, tbody: bodies, status: status };
        console.log(this.data);
      };
    }
    this.myInputVariable.nativeElement.value = "";
  }

  onUpload() {
    this.myInputVariable.nativeElement.value = "";
    var req = [];
    var title_list;
    const theader = this.data.theader;

    console.log(this.data.tbody);
    for (let item_arr of this.data.tbody) {
      let title = [];
    let score = [];
      var dataResult;
      for (let i = 3; i < item_arr.length - 1; i++) {
        title.push(theader[i]);
        score.push(item_arr[i]);
      }
      dataResult = {[item_arr[2]]: score };
      title_list = title;
      req.push({
        stu_code: item_arr[0],
        stu_name: item_arr[1],
        detail: dataResult,
        comment:item_arr[item_arr.length-1]
      });
    }
    this.model.stu_in_class = req;
    this.model.title = title_list;
    let data = {teach_code:this.teacher.teach_code, subject_detail : this.model};
    console.log(JSON.stringify(data));

    this.callApi.call("post", "scorestu/add", data).then(res => {
      console.log(res);
      if (res["status_code"] == 201) {
        console.log(JSON.stringify(this.model));
        alert("SUCCCESS");
        this._location.back();
      }
    });
  }
}
