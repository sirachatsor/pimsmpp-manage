import { HomeModule } from './home.module';

describe('HomePageModule', () => {
    let homeModule: HomeModule;

    beforeEach(() => {
        homeModule = new HomeModule();
    });

    it('should create an instance', () => {
        expect(homeModule).toBeTruthy();
    });
});
