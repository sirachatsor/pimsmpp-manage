import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {TeacherRoutingModule} from './teacher-routing.module';
import { TeacherComponent } from './teacher.component';
import { PageHeaderModule } from '../../shared';
import { DataTablesModule } from 'angular-datatables';
import { CallAPIService } from '../../call-api.service';

@NgModule({
    imports: [CommonModule, TeacherRoutingModule,PageHeaderModule,DataTablesModule],
    providers: [CallAPIService],
    declarations: [TeacherComponent]
})
export class TeacherModule {}
