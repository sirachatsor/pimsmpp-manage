import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { DataTableDirective } from "angular-datatables";
import { CallAPIService } from "../../call-api.service";
@Component({
  selector: "app-teacher",
  templateUrl: "./teacher.component.html",
  styleUrls: ["./teacher.component.scss"]
})
export class TeacherComponent implements OnInit {
  @ViewChild('inputFile') myInputVariable: ElementRef;
  constructor(private callApi : CallAPIService) {}
  datatableElement: DataTableDirective;
  data: any = { theader: [], tbody: [] };
  teach_arr = [];

  ngOnInit() {
    this.callApi.call("get","user/getalluser",{}).then(res =>{
      console.log(JSON.stringify(res["data"]));
      this.teach_arr = res["data"];
    })
  }

  changeListener(files: FileList) {
    console.log(files);
    if (files && files.length > 0) {
      let file: File = files.item(0);
      // console.log(file.name);
      // console.log(file.size);
      // console.log(file.type);
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = e => {
        let csv: any = reader.result;
        console.log(csv);
        let allTextLines = csv.split(/\r|\n|\r/);
        let headers = allTextLines[0].split(",");
        let bodies = [];

        for (let i = 1; i < allTextLines.length; i++) {
          // split content based on comma
          let data = allTextLines[i].split(",");
          if (data.length === headers.length) {
            let tarr = [];
            for (let j = 0; j < headers.length; j++) {
              tarr.push(data[j]);
            }
            bodies.push(tarr);
          }
        }
        // all rows in the csv file
        this.data = { theader: headers, tbody: bodies };
        
      };
    }
  }
  onUpload() {
    // console.log(this.data.tbody);
    this.myInputVariable.nativeElement.value = '';
    var req = []
    for (let item_arr of this.data.tbody) {
      req.push({"teach_code":item_arr[0],"teach_name":item_arr[1],"teach_lastname":item_arr[2]});
    }
    console.log(JSON.stringify(req));
    this.callApi.call("post","teacher/add",req).then(res=>{
      console.log(res)
      if(res['status_code'] == 201){
        this.data = { theader: [], tbody: [] };
        
        for (let item of req) {
          this.teach_arr.push(item);
        }
        
      }
    });
  }
}
