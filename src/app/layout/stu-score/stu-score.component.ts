import { Component, OnInit} from "@angular/core";
import { CallAPIService } from "../../call-api.service";
import { ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from "angular-datatables";


@Component({
    selector: 'app-stu-score',
    templateUrl: './stu-score.component.html',
    styleUrls: ['./stu-score.component.scss']
})
export class StuScoreComponent implements OnInit {
    constructor(private callApi : CallAPIService,private _Activatedroute: ActivatedRoute,private modalService: NgbModal) {}
    model:any;
    data:any
    closeResult: string;
    data_update:any;
    datatableElement: DataTableDirective;
   
    ngOnInit() {
        this._Activatedroute.queryParams.subscribe(queryParam => {
          this.data = {subject_code:queryParam["id"],class_year:queryParam["y"],term:queryParam["t"]};
        });
        this.callApi
          .call("post", "scorestu/getscore", this.data)
          .then(res => {
              this.model = res['data'][0];
              console.log(this.model);
          });
      }

      open(content,key) {
        this.data_update = this.model.stu_in_class[key];
    console.log('data_update',this.data_update);
          
        this.modalService.open(content).result.then((result) => {
            this.model.stu_in_class[key] = this.data_update;
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return  `with: ${reason}`;
        }
    }

    onUpdate(){

    }
}
