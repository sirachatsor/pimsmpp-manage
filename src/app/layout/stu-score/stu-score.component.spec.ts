import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StuScoreComponent } from './stu-score.component';

describe('BlankPageComponent', () => {
    let component: StuScoreComponent;
    let fixture: ComponentFixture<StuScoreComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [StuScoreComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(StuScoreComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
