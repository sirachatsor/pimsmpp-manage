import { StuScoreModule } from './stu-score.module';

describe('TeacherDetailModule', () => {
    let teacherModule: StuScoreModule;

    beforeEach(() => {
        teacherModule = new StuScoreModule();
    });

    it('should create an instance', () => {
        expect(teacherModule).toBeTruthy();
    });
});
