import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StuScoreComponent } from './stu-score.component';

const routes: Routes = [
    {
        path: '',
        component: StuScoreComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StuScoreRoutingModule {}
