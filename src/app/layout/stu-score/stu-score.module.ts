import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StuScoreRoutingModule } from './stu-score-routing.module';
import { StuScoreComponent } from './stu-score.component';
import { PageHeaderModule } from '../../shared';
import { DataTablesModule } from 'angular-datatables';
import { CallAPIService } from '../../call-api.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }   from '@angular/forms';




@NgModule({
    imports: [CommonModule, StuScoreRoutingModule ,PageHeaderModule,DataTablesModule,NgbModule,FormsModule],
    providers: [CallAPIService],
    declarations: [StuScoreComponent]
})
export class StuScoreModule {}
