import { SubjectModule } from './subject.module';

describe('ImportStudentModule', () => {
    let subjectModule: SubjectModule;

    beforeEach(() => {
        subjectModule = new SubjectModule();
    });

    it('should create an instance', () => {
        expect(subjectModule).toBeTruthy();
    });
});
