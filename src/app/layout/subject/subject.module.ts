import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubjectRoutingModule } from './subject-routing.module';
import { SubjectComponent } from './subject.component';
import { PageHeaderModule } from '../../shared';
import { FormsModule }   from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { CallAPIService } from '../../call-api.service';

@NgModule({
    imports: [CommonModule, SubjectRoutingModule,PageHeaderModule,FormsModule,DataTablesModule],
    providers: [CallAPIService],
    declarations: [SubjectComponent]
})
export class SubjectModule {}
