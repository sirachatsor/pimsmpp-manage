import { Component, OnInit } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { CallAPIService } from '../../call-api.service';

@Component({
    selector: 'app-subject',
    templateUrl: './subject.component.html',
    styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {
    model: any = {};
    datatableElement: DataTableDirective;
    subject_arr = [];
    constructor(private callApi : CallAPIService) {}

    ngOnInit() {
        this.callApi.call("get","subject/getallsubject",{}).then(res =>{
            // console.log(JSON.stringify(data['data']));
            this.subject_arr = res['data'];
            console.log("subject_arr",this.subject_arr);
        })
    }
    onSubmit() {
        console.log(this.model);
        this.callApi.call("post","subject/add",[this.model]).then(res =>{
            if(res['status_code'] == 201){
                let data_res = res['data'];
                console.log(data_res);
                this.subject_arr.push(data_res[0]);
                this.model = {};
            }
            // alert('SUCCESS!! :-)\n\n' + JSON.stringify(data));
        })
       
      }
}
