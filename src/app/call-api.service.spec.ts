import { TestBed } from '@angular/core/testing';

import { CallAPIService } from './call-api.service';

describe('CallAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CallAPIService = TestBed.get(CallAPIService);
    expect(service).toBeTruthy();
  });
});
