import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { routerTransition } from "../router.animations";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
  model: any = {};
  err_msg = "";
  url = "";
  data_arr = [
    {
      _id: "5c57679d097f6073014e8dc5",
      teach_code: "123456",
      teach_name: "พรสิริ",
      teach_lastname: "ชาติปรีชา"
    },
    {
      _id: "5c57679d097f6073014e8dc6",
      teach_code: "000001",
      teach_name: "อาจารย์คนที่ 1",
      teach_lastname: "นามสกุลอาจารย์คนที่ 1"
    },
    {
      _id: "5c57679d097f6073014e8dc7",
      teach_code: "000002",
      teach_name: "อาจารย์คนที่ 2",
      teach_lastname: "นามสกุลอาจารย์คนที่ 2"
    },
    {
      _id: "5c57679d097f6073014e8dc8",
      teach_code: "000003",
      teach_name: "อาจารย์คนที่ 3",
      teach_lastname: "นามสกุลอาจารย์คนที่ 3"
    },
    {
      _id: "5c57679d097f6073014e8dc9",
      teach_code: "000004",
      teach_name: "อาจารย์คนที่ 4",
      teach_lastname: "นามสกุลอาจารย์คนที่ 4"
    },
    {
      _id: "5c57679d097f6073014e8dca",
      teach_code: "000005",
      teach_name: "อาจารย์คนที่ 5",
      teach_lastname: "นามสกุลอาจารย์คนที่ 5"
    },
    {
      _id: "5c57679d097f6073014e8dcb",
      teach_code: "000006",
      teach_name: "อาจารย์คนที่ 6",
      teach_lastname: "นามสกุลอาจารย์คนที่ 6"
    },
    {
      _id: "5c57679d097f6073014e8dcc",
      teach_code: "000007",
      teach_name: "อาจารย์คนที่ 7",
      teach_lastname: "นามสกุลอาจารย์คนที่ 7"
    },
    {
      _id: "5c57679d097f6073014e8dcd",
      teach_code: "000008",
      teach_name: "อาจารย์คนที่ 8",
      teach_lastname: "นามสกุลอาจารย์คนที่ 8"
    },
    {
      _id: "5c57679d097f6073014e8dce",
      teach_code: "000009",
      teach_name: "อาจารย์คนที่ 9",
      teach_lastname: "นามสกุลอาจารย์คนที่ 9"
    },
    {
      _id: "5c57679d097f6073014e8dcf",
      teach_code: "000010",
      teach_name: "อาจารย์คนที่ 10",
      teach_lastname: "นามสกุลอาจารย์คนที่ 10"
    },
    {
      _id: "0000000000000000000000000",
      teach_code: "Administrator",
      teach_name: "Administrator",
      teach_lastname: "Administrator"
    }
  ];
  constructor(private translate: TranslateService, public router: Router) {
    this.translate.addLangs([
      "en",
      "fr",
      "ur",
      "es",
      "it",
      "fa",
      "de",
      "zh-CHS"
    ]);
    this.translate.setDefaultLang("en");
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(
      browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : "en"
    );
  }

  ngOnInit() {}

  onLoggedin() {
    if ((this.data_arr.some(x => x['teach_code'] === this.model.username) &&
      this.model.password == "password")||(this.model.username == "Administrator" && this.model.password == "P@ssw0rd")) {
        var data_login = this.data_arr.find(x => x['teach_code'] === this.model.username);

        localStorage.setItem("nameLogin", data_login.teach_name);
        localStorage.setItem("idLogin", data_login.teach_code);
        localStorage.setItem("isLoggedin", "true");
        
        this.err_msg = "";
        if(this.model.username == "Administrator"){
          this.router.navigate(['/home']);
        }else{
          this.router.navigate(['/teacher_detail']);
        }
        alert(this.url);
        
        console.log(data_login);
    } else {
          this.err_msg = "Username หรือ Password ไม่ถูกต้อง";
          return false;
        }

    // if (
    //   this.model.username == "Administrator" &&
    //   this.model.password == "P@ssw0rd"
    // ) {
    //   localStorage.setItem("isLoggedin", "true");
    //   this.err_msg = "";
    // } else {
    //   this.err_msg = "Username หรือ Password ไม่ถูกต้อง";
    //   return false;
    // }


  }
}
