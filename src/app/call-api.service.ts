import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
@Injectable()
export class CallAPIService {
  
  constructor(public http: HttpClient) {}
  apiUrl: any = "http://18.136.200.209/";
  data: any;

  call(type, url, value = {}) {
    // if (this.data) {
    //   return Promise.resolve(this.data);
    // }
    // const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');

    return new Promise(resolve => {
      if(type == "get"){
        this.http.get(this.apiUrl + url, {}).subscribe(
          res => {
            resolve(res);
          },
          error => {
            resolve(null);
          }
        );
      }else if(type == "post"){
        this.http.post(this.apiUrl + url, value).subscribe(
          res => {
            console.log("Call api success");
            resolve(res);
          },
          error => {
            console.log("Call api fail");
            resolve("ERROR");
          }
        );
      }
     
    });
  }
}
