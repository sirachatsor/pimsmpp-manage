(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["importStudent-import-student-module"],{

/***/ "./src/app/layout/importStudent/import-student-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/layout/importStudent/import-student-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: ImportStudentRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportStudentRoutingModule", function() { return ImportStudentRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _import_student_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./import-student.component */ "./src/app/layout/importStudent/import-student.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _import_student_component__WEBPACK_IMPORTED_MODULE_2__["ImportStudentComponent"]
    }
];
var ImportStudentRoutingModule = /** @class */ (function () {
    function ImportStudentRoutingModule() {
    }
    ImportStudentRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ImportStudentRoutingModule);
    return ImportStudentRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/importStudent/import-student.component.html":
/*!********************************************************************!*\
  !*** ./src/app/layout/importStudent/import-student.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\n  <app-page-header [heading]=\"'กรอกคะแนนนักศึกษา'\" [icon]=\"'fa-bar-chart-o'\">\n  </app-page-header>\n\n  <div class=\"row\">\n    <div class=\"col-lg-12\">\n      <div class=\"card mb-3\">\n        <div class=\"card-header\">{{\"อาจารย์ \"+teacher.teach_name+\" \"+teacher.teach_lastname}}\n          <input style=\"float: right;\" #inputFile type=\"file\" class=\"upload\" (change)=\"changeListener($event.target.files)\"></div>\n        <div class=\"card-body\">\n          <form name=\"form\" (ngSubmit)=\"f.form.valid && onUpload()\" #f=\"ngForm\" novalidate>\n            <div class=\"row\">\n              <div class=\"form-group col-md-5\">\n                <label>วิชาที่สอน</label>\n                <select class=\"form-control\" name=\"subject_code\" [(ngModel)]=\"model.subject_code\" #subjectCode=\"ngModel\"\n                  [ngClass]=\"{\n                  'is-invalid': f.submitted && subjectCode.invalid\n                }\"\n                  required>\n                  <option *ngFor=\"let item of subject\" value=\"{{item.subject_code+'-'+item.subject_name}}\">{{item.subject_code+\" -\n                    \"+item.subject_name}}</option>\n                </select>\n                <div *ngIf=\"f.submitted && subjectCode.invalid\" class=\"invalid-feedback\">\n                  <div *ngIf=\"subjectCode.errors.required\">\n                    รหัสวิชาไม่ควรว่าง\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group col-md-3\">\n                <label for=\"subjectCode\">ปีการศึกษา</label>\n                <select class=\"form-control\" name=\"class_year\" [(ngModel)]=\"model.class_year\" #classYear=\"ngModel\"\n                  [ngClass]=\"{\n                  'is-invalid': f.submitted && classYear.invalid\n                }\"\n                  required>\n                  <option value=\"2562\">2562</option>\n                  <option value=\"2561\">2561</option>\n                  <option value=\"2560\">2560</option>\n                  <option value=\"2559\">2559</option>\n                </select>\n                <div *ngIf=\"f.submitted && classYear.invalid\" class=\"invalid-feedback\">\n                  <div *ngIf=\"classYear.errors.required\">\n                    ปีการศึกษาไม่ควรว่าง\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group col-md-3\">\n                <label>ภาคการศึกษา</label>\n                <select class=\"form-control\" name=\"term\" [(ngModel)]=\"model.term\" #Term=\"ngModel\" [ngClass]=\"{\n                      'is-invalid': f.submitted && Term.invalid\n                    }\"\n                  required>\n                  <option value=\"1\">1</option>\n                  <option value=\"2\">2</option>\n                  <option value=\"3\">3</option>\n                  <option value=\"4\">4</option>\n                </select>\n                <div *ngIf=\"f.submitted && Term.invalid\" class=\"invalid-feedback\">\n                  <div *ngIf=\"Term.errors.required\">\n                    ภาคการศึกษาไม่ควรว่าง\n                  </div>\n                </div>\n              </div>\n            </div>\n          \n          <table datatable class=\"row-border hover\">\n            <thead *ngIf=\"show_tbl\">\n              <th class=\"text-center\"> สถานะ </th>\n              <th class=\"text-center\" *ngFor=\"let item of data.theader\">{{item}}</th>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let items of data.tbody | keyvalue\">\n                  <td class=\"text-center\"><i class=\"{{data.status[items.key]}}\" ></i></td>\n                <td class=\"text-center\" *ngFor=\"let item of items.value\">{{item}}</td>\n              </tr>\n            </tbody>\n            <!-- <tfoot>\n                  <tr>\n                    <th><input type=\"text\" placeholder=\"Search ID\" name=\"search-id\"/></th>\n                    <th><input type=\"text\" placeholder=\"Search first name\" name=\"search-first-name\"/></th>\n                    <th><input type=\"text\" placeholder=\"Search last name\" name=\"search-last-name\"/></th>\n                  </tr>\n                </tfoot> -->\n          </table>\n          <button [disabled]=\"show_msg\" style=\"width: 100%;\">upload</button>\n          <p>\n          <div class=\"text-center\" *ngIf=\"show_msg\" class=\"alert alert-danger mb-0\">{{msg}}</div>\n        </form>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/importStudent/import-student.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/layout/importStudent/import-student.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".co-red {\n  color: red; }\n\n.co-green {\n  color: green; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zaXJhY2hhdHNvci9NeUNvZGUvcGltc21wcF9tYW5hZ2Uvc3JjL2FwcC9sYXlvdXQvaW1wb3J0U3R1ZGVudC9pbXBvcnQtc3R1ZGVudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQVMsRUFDWjs7QUFDRDtFQUNJLGFBQVcsRUFDZCIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9pbXBvcnRTdHVkZW50L2ltcG9ydC1zdHVkZW50LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvLXJlZHtcbiAgICBjb2xvcjpyZWQ7XG59XG4uY28tZ3JlZW57XG4gICAgY29sb3I6Z3JlZW47XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/importStudent/import-student.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/layout/importStudent/import-student.component.ts ***!
  \******************************************************************/
/*! exports provided: ImportStudentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportStudentComponent", function() { return ImportStudentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ImportStudentComponent = /** @class */ (function () {
    function ImportStudentComponent(_Activatedroute, callApi, _location) {
        this._Activatedroute = _Activatedroute;
        this.callApi = callApi;
        this._location = _location;
        this.data = { theader: [], tbody: [] };
        this.model = {};
        this.show_tbl = false;
        this.show_msg = false;
        this.msg = "";
    }
    ImportStudentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._Activatedroute.queryParams.subscribe(function (queryParam) {
            _this.params = queryParam["id"];
        });
        this.callApi
            .call("post", "getaddscorestudent", { teach_code: this.params })
            .then(function (res) {
            console.log("res", res);
            _this.jsonData = res["data"];
            _this.teacher = _this.jsonData["teacher"][0];
            _this.subject = _this.jsonData["subject"];
            console.log(_this.teacher);
        });
    };
    ImportStudentComponent.prototype.changeListener = function (files) {
        var _this = this;
        console.log(files);
        if (files && files.length > 0) {
            var file = files.item(0);
            console.log(file.name);
            console.log(file.size);
            console.log(file.type);
            var reader_1 = new FileReader();
            reader_1.readAsText(file);
            reader_1.onload = function (e) {
                _this.msg = "";
                _this.show_msg = false;
                var csv = reader_1.result;
                console.log(csv);
                var allTextLines = csv.split(/\r|\n|\r/);
                var headers = allTextLines[0].split(",");
                var bodies = [];
                var status = [];
                var _loop_1 = function (i) {
                    // split content based on comma
                    var data = allTextLines[i].split(",");
                    validate_score = "fa fa-check co-green";
                    if (data.length === headers.length) {
                        var tarr_1 = [];
                        for (var j = 0; j < headers.length; j++) {
                            // console.log("key is  : " + j + " value is : " + data[j]);
                            if ((j >= 3 && j < headers.length - 1 && data[j] > 100) ||
                                data[j] < 0) {
                                _this.msg =
                                    "*** คะแนนนักศึกษารหัสที่ " +
                                        data[0] +
                                        " ไม่ถูกต้อง รบกวนตรวจสอบข้อมูลและอัพโหลดไฟล์อีกครั้ง ***";
                                _this.show_msg = true;
                                console.log("คะแนนผิดพลาด รบกวนตรวจสอบข้อมูลและ อัพโหลดไฟล์อีกครั้ง");
                                validate_score = "fa fa-times co-red";
                            }
                            else if (j == 0) {
                                // console.log(bodies.length);
                                // for (let a = 0; a < bodies.length; a++) {
                                //   if(bodies[a][0] ==)
                                //   console.log()
                                //   console.log(bodies[a].includes(data[0]));
                                // }
                            }
                            // console.log(bodies.includes(data[0]));
                            tarr_1.push(data[j]);
                            // console.log(data[0]);
                            // var id =  data[0].toString();
                        }
                        status.push(validate_score);
                        // log each row to see output
                        console.log(tarr_1[0]);
                        console.log(bodies.some(function (x) { return x[0] === tarr_1[0]; }));
                        if (bodies.some(function (x) { return x[0] === tarr_1[0]; })) {
                            _this.msg =
                                "*** พบรหัสนักศึกษา " +
                                    data[0] +
                                    " ซ้ำ รบกวนตรวจสอบข้อมูลและอัพโหลดไฟล์อีกครั้ง ***";
                            _this.show_msg = true;
                        }
                        else if (bodies.some(function (x) { return x[1] === tarr_1[1]; })) {
                            _this.msg =
                                "*** พบชื่อนักศึกษา " +
                                    data[1] +
                                    " ซ้ำ รบกวนตรวจสอบข้อมูลและอัพโหลดไฟล์อีกครั้ง ***";
                            _this.show_msg = true;
                        }
                        console.log("Check duplicate");
                        console.log("End check dupplicate");
                        bodies.push(tarr_1);
                    }
                };
                var validate_score;
                for (var i = 1; i < allTextLines.length; i++) {
                    _loop_1(i);
                }
                // all rows in the csv file
                _this.show_tbl = true;
                _this.data = { theader: headers, tbody: bodies, status: status };
                console.log(_this.data);
            };
        }
        this.myInputVariable.nativeElement.value = "";
    };
    ImportStudentComponent.prototype.onUpload = function () {
        var _this = this;
        var _a;
        this.myInputVariable.nativeElement.value = "";
        var req = [];
        var title_list;
        var theader = this.data.theader;
        console.log(this.data.tbody);
        for (var _i = 0, _b = this.data.tbody; _i < _b.length; _i++) {
            var item_arr = _b[_i];
            var title = [];
            var score = [];
            var dataResult;
            for (var i = 3; i < item_arr.length - 1; i++) {
                title.push(theader[i]);
                score.push(item_arr[i]);
            }
            dataResult = (_a = {}, _a[item_arr[2]] = score, _a);
            title_list = title;
            req.push({
                stu_code: item_arr[0],
                stu_name: item_arr[1],
                detail: dataResult,
                comment: item_arr[item_arr.length - 1]
            });
        }
        this.model.stu_in_class = req;
        this.model.title = title_list;
        var data = { teach_code: this.teacher.teach_code, subject_detail: this.model };
        console.log(JSON.stringify(data));
        this.callApi.call("post", "scorestu/add", data).then(function (res) {
            console.log(res);
            if (res["status_code"] == 201) {
                console.log(JSON.stringify(_this.model));
                alert("SUCCCESS");
                _this._location.back();
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])("inputFile"),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ImportStudentComponent.prototype, "myInputVariable", void 0);
    ImportStudentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-import-student",
            template: __webpack_require__(/*! ./import-student.component.html */ "./src/app/layout/importStudent/import-student.component.html"),
            styles: [__webpack_require__(/*! ./import-student.component.scss */ "./src/app/layout/importStudent/import-student.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _call_api_service__WEBPACK_IMPORTED_MODULE_2__["CallAPIService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]])
    ], ImportStudentComponent);
    return ImportStudentComponent;
}());



/***/ }),

/***/ "./src/app/layout/importStudent/import-student.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/layout/importStudent/import-student.module.ts ***!
  \***************************************************************/
/*! exports provided: ImportStudentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImportStudentModule", function() { return ImportStudentModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _import_student_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./import-student-routing.module */ "./src/app/layout/importStudent/import-student-routing.module.ts");
/* harmony import */ var _import_student_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./import-student.component */ "./src/app/layout/importStudent/import-student.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var ImportStudentModule = /** @class */ (function () {
    function ImportStudentModule() {
    }
    ImportStudentModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _import_student_routing_module__WEBPACK_IMPORTED_MODULE_2__["ImportStudentRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"]],
            declarations: [_import_student_component__WEBPACK_IMPORTED_MODULE_3__["ImportStudentComponent"]],
            providers: [_call_api_service__WEBPACK_IMPORTED_MODULE_6__["CallAPIService"]]
        })
    ], ImportStudentModule);
    return ImportStudentModule;
}());



/***/ })

}]);
//# sourceMappingURL=importStudent-import-student-module.js.map