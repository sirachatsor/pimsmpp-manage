(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["teacher-teacher-module"],{

/***/ "./src/app/layout/teacher/teacher-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/teacher/teacher-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: TeacherRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherRoutingModule", function() { return TeacherRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _teacher_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./teacher.component */ "./src/app/layout/teacher/teacher.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _teacher_component__WEBPACK_IMPORTED_MODULE_2__["TeacherComponent"]
    }
];
var TeacherRoutingModule = /** @class */ (function () {
    function TeacherRoutingModule() {
    }
    TeacherRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], TeacherRoutingModule);
    return TeacherRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/teacher/teacher.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/teacher/teacher.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\n  <app-page-header [heading]=\"'จัดการอาจารย์ผู้สอนประจำวิชา'\" [icon]=\"'fa-bar-chart-o'\">\n  </app-page-header>\n\n  <div class=\"row\">\n    <div class=\"col-lg-12\">\n      <div class=\"card mb-3\">\n        <div class=\"card-header\">เพิ่มอาจารย์ <input #inputFile style=\"float: right;\" type=\"file\" class=\"upload\" (change)=\"changeListener($event.target.files)\"></div>\n        <div class=\"card-body\">\n          <table datatable class=\"row-border hover\">\n            <thead>\n              <th *ngFor=\"let item of data.theader\">{{item}}</th>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let items of data.tbody\">\n                <td *ngFor=\"let item of items\">{{item}}</td>\n              </tr>\n            </tbody>\n          </table>\n          <a (click)=\"onUpload()\"><button style=\"width: 100%;\">upload</button></a>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-lg-12\">\n      <div class=\"card mb-3\">\n        <div class=\"card-header\">รายชื่ออาจารย์</div>\n        <div class=\"card-body\">\n          <table datatable class=\"row-border hover\">\n            <thead>\n              <tr>\n                <th>รหัส</th>\n                <th>ชื่อ</th>\n                <th>นามสกุล</th>\n                <th>&nbsp;</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let teach of teach_arr\">\n                <td>{{teach.teach_code}}</td>\n                <td>{{teach.teach_name}}</td>\n                <td>{{teach.teach_lastname}}</td>\n                <td>\n                  <a [routerLink]=\"['/imp-stu']\" [queryParams]=\"{id: teach.teach_code}\">\n                    กรอกคะแนน\n                  </a>\n                </td>\n              </tr>\n            </tbody>\n            <!-- <tfoot>\n                      <tr>\n                        <th><input type=\"text\" placeholder=\"Search ID\" name=\"search-id\"/></th>\n                        <th><input type=\"text\" placeholder=\"Search first name\" name=\"search-first-name\"/></th>\n                        <th><input type=\"text\" placeholder=\"Search last name\" name=\"search-last-name\"/></th>\n                      </tr>\n                    </tfoot> -->\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/teacher/teacher.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/teacher/teacher.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC90ZWFjaGVyL3RlYWNoZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/teacher/teacher.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/teacher/teacher.component.ts ***!
  \*****************************************************/
/*! exports provided: TeacherComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherComponent", function() { return TeacherComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TeacherComponent = /** @class */ (function () {
    function TeacherComponent(callApi) {
        this.callApi = callApi;
        this.data = { theader: [], tbody: [] };
        this.teach_arr = [];
    }
    TeacherComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.callApi.call("get", "user/getalluser", {}).then(function (res) {
            console.log(JSON.stringify(res["data"]));
            _this.teach_arr = res["data"];
        });
    };
    TeacherComponent.prototype.changeListener = function (files) {
        var _this = this;
        console.log(files);
        if (files && files.length > 0) {
            var file = files.item(0);
            // console.log(file.name);
            // console.log(file.size);
            // console.log(file.type);
            var reader_1 = new FileReader();
            reader_1.readAsText(file);
            reader_1.onload = function (e) {
                var csv = reader_1.result;
                console.log(csv);
                var allTextLines = csv.split(/\r|\n|\r/);
                var headers = allTextLines[0].split(",");
                var bodies = [];
                for (var i = 1; i < allTextLines.length; i++) {
                    // split content based on comma
                    var data = allTextLines[i].split(",");
                    if (data.length === headers.length) {
                        var tarr = [];
                        for (var j = 0; j < headers.length; j++) {
                            tarr.push(data[j]);
                        }
                        bodies.push(tarr);
                    }
                }
                // all rows in the csv file
                _this.data = { theader: headers, tbody: bodies };
            };
        }
    };
    TeacherComponent.prototype.onUpload = function () {
        var _this = this;
        // console.log(this.data.tbody);
        this.myInputVariable.nativeElement.value = '';
        var req = [];
        for (var _i = 0, _a = this.data.tbody; _i < _a.length; _i++) {
            var item_arr = _a[_i];
            req.push({ "teach_code": item_arr[0], "teach_name": item_arr[1], "teach_lastname": item_arr[2] });
        }
        console.log(JSON.stringify(req));
        this.callApi.call("post", "teacher/add", req).then(function (res) {
            console.log(res);
            if (res['status_code'] == 201) {
                _this.data = { theader: [], tbody: [] };
                for (var _i = 0, req_1 = req; _i < req_1.length; _i++) {
                    var item = req_1[_i];
                    _this.teach_arr.push(item);
                }
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('inputFile'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], TeacherComponent.prototype, "myInputVariable", void 0);
    TeacherComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "app-teacher",
            template: __webpack_require__(/*! ./teacher.component.html */ "./src/app/layout/teacher/teacher.component.html"),
            styles: [__webpack_require__(/*! ./teacher.component.scss */ "./src/app/layout/teacher/teacher.component.scss")]
        }),
        __metadata("design:paramtypes", [_call_api_service__WEBPACK_IMPORTED_MODULE_1__["CallAPIService"]])
    ], TeacherComponent);
    return TeacherComponent;
}());



/***/ }),

/***/ "./src/app/layout/teacher/teacher.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/teacher/teacher.module.ts ***!
  \**************************************************/
/*! exports provided: TeacherModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeacherModule", function() { return TeacherModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _teacher_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./teacher-routing.module */ "./src/app/layout/teacher/teacher-routing.module.ts");
/* harmony import */ var _teacher_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teacher.component */ "./src/app/layout/teacher/teacher.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var TeacherModule = /** @class */ (function () {
    function TeacherModule() {
    }
    TeacherModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _teacher_routing_module__WEBPACK_IMPORTED_MODULE_2__["TeacherRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"]],
            providers: [_call_api_service__WEBPACK_IMPORTED_MODULE_6__["CallAPIService"]],
            declarations: [_teacher_component__WEBPACK_IMPORTED_MODULE_3__["TeacherComponent"]]
        })
    ], TeacherModule);
    return TeacherModule;
}());



/***/ })

}]);
//# sourceMappingURL=teacher-teacher-module.js.map