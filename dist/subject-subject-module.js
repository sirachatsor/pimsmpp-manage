(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["subject-subject-module"],{

/***/ "./src/app/layout/subject/subject-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/subject/subject-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: SubjectRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubjectRoutingModule", function() { return SubjectRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _subject_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./subject.component */ "./src/app/layout/subject/subject.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: "",
        component: _subject_component__WEBPACK_IMPORTED_MODULE_2__["SubjectComponent"]
    }
];
var SubjectRoutingModule = /** @class */ (function () {
    function SubjectRoutingModule() {
    }
    SubjectRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SubjectRoutingModule);
    return SubjectRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/subject/subject.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/subject/subject.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\n  <app-page-header [heading]=\"'จัดการรายวิชา'\" [icon]=\"'fa-bar-chart-o'\">\n  </app-page-header>\n  <div class=\"row\">\n    <div class=\"col-lg-12\">\n      <div class=\"card mb-3\">\n        <div class=\"card-header\">เพิ่มรายวิชา</div>\n        <div class=\"card-body\">\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n              <form name=\"form\" (ngSubmit)=\"f.form.valid && onSubmit()\" #f=\"ngForm\" novalidate>\n                <div class=\"row\">\n                  <div class=\"form-group col-md-3\">\n                    <label for=\"subjectCode\">รหัสวิชา</label>\n                    <input type=\"text\" class=\"form-control\" name=\"subject_code\" [(ngModel)]=\"model.subject_code\"\n                      #subjectCode=\"ngModel\" [ngClass]=\"{\n                            'is-invalid': f.submitted && subjectCode.invalid\n                          }\"\n                      required />\n                    <div *ngIf=\"f.submitted && subjectCode.invalid\" class=\"invalid-feedback\">\n                      <div *ngIf=\"subjectCode.errors.required\">\n                        รหัสวิชาไม่ควรว่าง\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group col-md-9\">\n                    <label for=\"lastName\">ชื่อวิชา</label>\n                    <input type=\"text\" class=\"form-control\" name=\"subject_name\" [(ngModel)]=\"model.subject_name\" #subjectName=\"ngModel\"\n                      [ngClass]=\"{\n                            'is-invalid': f.submitted && subjectName.invalid\n                          }\"\n                      required />\n                    <div *ngIf=\"f.submitted && subjectName.invalid\" class=\"invalid-feedback\">\n                      <div *ngIf=\"subjectName.errors.required\">\n                        ชื่อวิชาไม่ควรว่าง\n                      </div>\n                    </div>\n                  </div>\n                </div>\n\n                <div class=\"form-group\">\n                  <button class=\"btn btn-primary\">เพิ่ม</button>\n                </div>\n              </form>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n      <div class=\"col-lg-12\">\n        <div class=\"card mb-3\">\n          <div class=\"card-header\">รายวิชา</div>\n          <div class=\"card-body\">\n              <table datatable   class=\"row-border hover\">\n                  <thead>\n                    <tr>\n                      <th>รหัสวิชา</th>\n                      <th>ชื่อวิชา</th>\n                    </tr>\n                  </thead>\n                  <tbody>\n                      <tr *ngFor=\"let subject of subject_arr\">\n                          <td>{{subject.subject_code}}</td>\n                          <td>{{subject.subject_name}}</td>\n                        </tr>\n                    \n                  </tbody>\n                  <!-- <tfoot>\n                      <tr>\n                        <th><input type=\"text\" placeholder=\"Search ID\" name=\"search-id\"/></th>\n                        <th><input type=\"text\" placeholder=\"Search first name\" name=\"search-first-name\"/></th>\n                        <th><input type=\"text\" placeholder=\"Search last name\" name=\"search-last-name\"/></th>\n                      </tr>\n                    </tfoot> -->\n                </table>\n          </div>\n        </div>\n      </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/layout/subject/subject.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/subject/subject.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zdWJqZWN0L3N1YmplY3QuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/layout/subject/subject.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/subject/subject.component.ts ***!
  \*****************************************************/
/*! exports provided: SubjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubjectComponent", function() { return SubjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubjectComponent = /** @class */ (function () {
    function SubjectComponent(callApi) {
        this.callApi = callApi;
        this.model = {};
        this.subject_arr = [];
    }
    SubjectComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.callApi.call("get", "subject/getallsubject", {}).then(function (res) {
            // console.log(JSON.stringify(data['data']));
            _this.subject_arr = res['data'];
            console.log("subject_arr", _this.subject_arr);
        });
    };
    SubjectComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.model);
        this.callApi.call("post", "subject/add", [this.model]).then(function (res) {
            if (res['status_code'] == 201) {
                var data_res = res['data'];
                console.log(data_res);
                _this.subject_arr.push(data_res[0]);
                _this.model = {};
            }
            // alert('SUCCESS!! :-)\n\n' + JSON.stringify(data));
        });
    };
    SubjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-subject',
            template: __webpack_require__(/*! ./subject.component.html */ "./src/app/layout/subject/subject.component.html"),
            styles: [__webpack_require__(/*! ./subject.component.scss */ "./src/app/layout/subject/subject.component.scss")]
        }),
        __metadata("design:paramtypes", [_call_api_service__WEBPACK_IMPORTED_MODULE_1__["CallAPIService"]])
    ], SubjectComponent);
    return SubjectComponent;
}());



/***/ }),

/***/ "./src/app/layout/subject/subject.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/subject/subject.module.ts ***!
  \**************************************************/
/*! exports provided: SubjectModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubjectModule", function() { return SubjectModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _subject_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./subject-routing.module */ "./src/app/layout/subject/subject-routing.module.ts");
/* harmony import */ var _subject_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./subject.component */ "./src/app/layout/subject/subject.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var SubjectModule = /** @class */ (function () {
    function SubjectModule() {
    }
    SubjectModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _subject_routing_module__WEBPACK_IMPORTED_MODULE_2__["SubjectRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_6__["DataTablesModule"]],
            providers: [_call_api_service__WEBPACK_IMPORTED_MODULE_7__["CallAPIService"]],
            declarations: [_subject_component__WEBPACK_IMPORTED_MODULE_3__["SubjectComponent"]]
        })
    ], SubjectModule);
    return SubjectModule;
}());



/***/ })

}]);
//# sourceMappingURL=subject-subject-module.js.map