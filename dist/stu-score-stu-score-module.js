(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["stu-score-stu-score-module"],{

/***/ "./src/app/layout/stu-score/stu-score-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/stu-score/stu-score-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: StuScoreRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StuScoreRoutingModule", function() { return StuScoreRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _stu_score_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stu-score.component */ "./src/app/layout/stu-score/stu-score.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _stu_score_component__WEBPACK_IMPORTED_MODULE_2__["StuScoreComponent"]
    }
];
var StuScoreRoutingModule = /** @class */ (function () {
    function StuScoreRoutingModule() {
    }
    StuScoreRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], StuScoreRoutingModule);
    return StuScoreRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/stu-score/stu-score.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layout/stu-score/stu-score.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div [@routerTransition]> -->\n    <app-page-header [heading]=\"'คะแนนนักศึกษา'\" [icon]=\"'fa-bar-chart-o'\">\n      </app-page-header>\n    <div class=\"card mb-3\">\n        <div class=\"card-header\">{{model.subject_code+\" ปีการศึกษา \"+model.class_year+\" ภาคการเรียนที่ \"+model.term}} </div>\n        <div class=\"card-body\">\n            <table datatable class=\"row-border hover\">\n                <thead >\n                  <th class=\"text-center\"> รหัสนักศึกษา </th>\n                  <th class=\"text-center\"> ชื่อ-นามสกุล </th>\n                  <th *ngFor=\"let title of model.title\" >{{title}}</th>\n                  <th class=\"text-center\"> คำแนะนำ </th>\n                  <!-- <th class=\"text-center\" *ngFor=\"let item of data.theader\">{{item}}</th> -->\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let items of model.stu_in_class\">\n                      <td class=\"text-center\">{{items.stu_code}}</td>\n                      <td class=\"text-center\">{{items.stu_name}}</td>\n                      <td class=\"text-center\" *ngFor=\"let item of items.detail.pre\">{{item}}</td>\n                      <td class=\"text-center\">{{items.comment}}</td>\n                  </tr> \n                </tbody>\n                <!-- <tfoot>\n                      <tr>\n                        <th><input type=\"text\" placeholder=\"Search ID\" name=\"search-id\"/></th>\n                        <th><input type=\"text\" placeholder=\"Search first name\" name=\"search-first-name\"/></th>\n                        <th><input type=\"text\" placeholder=\"Search last name\" name=\"search-last-name\"/></th>\n                      </tr>\n                    </tfoot> -->\n              </table>\n            </div>\n        </div>\n  \n<!-- </div> -->\n"

/***/ }),

/***/ "./src/app/layout/stu-score/stu-score.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/layout/stu-score/stu-score.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".co-red {\n  color: red; }\n\n.co-green {\n  color: green; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zaXJhY2hhdHNvci9NeUNvZGUvcGltc21wcF9tYW5hZ2Uvc3JjL2FwcC9sYXlvdXQvc3R1LXNjb3JlL3N0dS1zY29yZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQVMsRUFDWjs7QUFDRDtFQUNJLGFBQVcsRUFDZCIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zdHUtc2NvcmUvc3R1LXNjb3JlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvLXJlZHtcbiAgICBjb2xvcjpyZWQ7XG59XG4uY28tZ3JlZW57XG4gICAgY29sb3I6Z3JlZW47XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/stu-score/stu-score.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/stu-score/stu-score.component.ts ***!
  \*********************************************************/
/*! exports provided: StuScoreComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StuScoreComponent", function() { return StuScoreComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StuScoreComponent = /** @class */ (function () {
    function StuScoreComponent(callApi, _Activatedroute) {
        this.callApi = callApi;
        this._Activatedroute = _Activatedroute;
    }
    StuScoreComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._Activatedroute.queryParams.subscribe(function (queryParam) {
            _this.data = { subject_code: queryParam["id"], class_year: queryParam["y"], term: queryParam["t"] };
        });
        this.callApi
            .call("post", "scorestu/getscore", this.data)
            .then(function (res) {
            _this.model = res['data'][0];
            console.log(_this.model);
        });
    };
    StuScoreComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-stu-score',
            template: __webpack_require__(/*! ./stu-score.component.html */ "./src/app/layout/stu-score/stu-score.component.html"),
            styles: [__webpack_require__(/*! ./stu-score.component.scss */ "./src/app/layout/stu-score/stu-score.component.scss")]
        }),
        __metadata("design:paramtypes", [_call_api_service__WEBPACK_IMPORTED_MODULE_1__["CallAPIService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], StuScoreComponent);
    return StuScoreComponent;
}());



/***/ }),

/***/ "./src/app/layout/stu-score/stu-score.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/stu-score/stu-score.module.ts ***!
  \******************************************************/
/*! exports provided: StuScoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StuScoreModule", function() { return StuScoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _stu_score_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./stu-score-routing.module */ "./src/app/layout/stu-score/stu-score-routing.module.ts");
/* harmony import */ var _stu_score_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./stu-score.component */ "./src/app/layout/stu-score/stu-score.component.ts");
/* harmony import */ var _shared__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared */ "./src/app/shared/index.ts");
/* harmony import */ var angular_datatables__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-datatables */ "./node_modules/angular-datatables/index.js");
/* harmony import */ var _call_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../call-api.service */ "./src/app/call-api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var StuScoreModule = /** @class */ (function () {
    function StuScoreModule() {
    }
    StuScoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _stu_score_routing_module__WEBPACK_IMPORTED_MODULE_2__["StuScoreRoutingModule"], _shared__WEBPACK_IMPORTED_MODULE_4__["PageHeaderModule"], angular_datatables__WEBPACK_IMPORTED_MODULE_5__["DataTablesModule"]],
            providers: [_call_api_service__WEBPACK_IMPORTED_MODULE_6__["CallAPIService"]],
            declarations: [_stu_score_component__WEBPACK_IMPORTED_MODULE_3__["StuScoreComponent"]]
        })
    ], StuScoreModule);
    return StuScoreModule;
}());



/***/ })

}]);
//# sourceMappingURL=stu-score-stu-score-module.js.map